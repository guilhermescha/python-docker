# Hello Python

Hello! This is a simple Python script designed to run in a Docker Container. This script has two different outputs, using or not environment variables.

- **Using Environment Variables**
  - Input: `docker run hello-python -e TEST=<value>`
  - Output: `Hello! TEST env value: <value>`
  
- **Not Using Environment Variables**
  - Input: `docker run hello-python`
  - Output: `Hello! TEST env value: default`